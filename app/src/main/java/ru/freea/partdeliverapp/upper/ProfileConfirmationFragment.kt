package ru.freea.partdeliverapp.upper

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile_confirmation.*
import kotlinx.android.synthetic.main.info_last_address.*
import ru.freea.partdeliverapp.MainActivity
import ru.freea.partdeliverapp.R
import ru.freea.partdeliverapp.model.Delivery
import ru.freea.partdeliverapp.model.User
import ru.freea.partdeliverapp.persistence.ModelPreferencesManager
import ru.freea.partdeliverapp.tools.UserNotifier
import ru.freea.partdeliverapp.tools.UserObserver
import ru.freea.partdeliverapp.tools.UserTools
import java.lang.Exception


class ProfileConfirmationFragment : Fragment(), UserTools {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile_confirmation, container, false)
    }

    private val index = 1
    private var observer: UserObserver? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            try {
                (it as MainActivity).notifier = object : UserNotifier {
                    override fun notify(user: User?) {
                        println("user instance = $user")
                        user?.let {
                            Observable.just(user)
                                .map {
                                    val data: MutableList<Delivery> =
                                        ModelPreferencesManager.getArray("data")
                                    data.let { list ->
                                        if (list.size > 0) {
                                            user.location?.apply {
                                                list.last().let { last ->
                                                    street = last.addressFrom
                                                    city = null
                                                    state = null
                                                    country = null
                                                }
                                            }
                                        }
                                    }
                                    return@map user
                                }
                                .observeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    println(user)
                                    loadProfile(
                                        requireActivity(),
                                        user,
                                        image_user_profile,
                                        text_username,
                                        text_phone_number,
                                        text_address
                                    )
                                }, { err -> err.printStackTrace() })
                        }

                        tap_submit_profile?.let { b ->
                            b.setOnClickListener { v ->
                                NavigationPurpose.purpose[index + 1]?.let { p ->
                                    val action = ProfileConfirmationFragmentDirections
                                        .actionProfileOrder(nextPurpose = p)
                                    Navigation.findNavController(v).navigate(action)
                                }
                            }
                        }
                    }
                }

                tap_package?.apply {
                    setOnClickListener { btn ->
                        val action = ProfileConfirmationFragmentDirections
                            .actionProfileOrder(nextPurpose = "myOrders")
                        btn.findNavController().navigate(action)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        observer?.apply { observe() }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        observer = try {
            context as UserObserver
        } catch (e: Exception) {
            null
        }
    }

}
