package ru.freea.partdeliverapp.upper

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PurposeArg(
    var title: String = "⏳",
    var weightView: Boolean = false,
    var clearButton: Boolean = false,
    var expressDeliver: Boolean = false,
    var whereTaken: Boolean = false,
    var whereDeliver: Boolean = false,
    var pricePanel: Boolean = false,
    var closeIcon: Boolean = false,
    var backIcon: Boolean = false,
    var ordersPreview: Boolean = false,
    var cancelButton: Boolean = false,
    var index: Int? = null
) : Parcelable {
    override fun toString(): String {
        return "title = $title,\n" +
                "weightView = $weightView,\n" +
                "clearButton = $clearButton,\n" +
                "expressDeliver = $expressDeliver,\n" +
                "whereTaken = $whereTaken,\n" +
                "whereDeliver = $whereDeliver,\n" +
                "pricePanel = $pricePanel,\n" +
                "closeIcon = $closeIcon,\n" +
                "backIcon = $backIcon,\n" +
                "ordersPreview = $ordersPreview,\n" +
                "cancelButton = $cancelButton,\n" +
                "index = $index"
    }
}