package ru.freea.partdeliverapp.upper

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import io.reactivex.rxjava3.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_order_process.*
import kotlinx.android.synthetic.main.purshase_container.*
import ru.freea.partdeliverapp.R
import ru.freea.partdeliverapp.model.Delivery
import ru.freea.partdeliverapp.tools.*

class OrderProcessFragment : Fragment(), ChipTools, DeliveryTools, WeightTools, NextPanelBehavior,
    SwitchTools, OrderTools {

    private var currentPrice: Double? = null

    private var hasExpress: Boolean = false

    private val subjectWeight: PublishSubject<Double> = PublishSubject.create()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order_process, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        evaluateWeight(requireActivity(), subjectWeight, viewModelStore)
        subjectWeight.subscribe {
            currentPrice = it
            setPrice(it)
            setSwitchPrice(it)
        }
        loadPanel(requireActivity())
        arguments?.apply {
            val purpose = getString("nextPurpose")
            val delivery: Delivery? = getParcelable("delivery")
            val index = NavigationPurpose.findIndex(purpose ?: "")
            if (index == 2) {
                setExpress(false)
                setSwitchPrice(0.0)
            }
            println("---purpose = ($purpose)\n---delivery = ($delivery)\n---index = ($index)")
            println("---purpose = ($purpose)\n---delivery = ($delivery)\n---index = ($index)")
            setNextDelivery(delivery)
            setDeliveryForOrder(delivery)
            purpose?.let {
                val arg: PurposeArg = NavigationPurpose.findPurpose(it)
                handleToolbar(
                    purpose = arg,
                    toolbar = panelToolbar,
                    toHidePanel = arrayOf(panel_sender_info, panel_receiver_info),
                    toHideTextView = arrayOf(text_price),
                    toUncheckSwitch = arrayOf(panel_express_delivery.findViewById(R.id.cbExpressDelivery)),
                    index
                )
                val map = mapOf<View?, Boolean>(
                    panel_express_delivery to arg.expressDeliver,
                    panelPurshase to arg.pricePanel,
                    panelContent to arg.ordersPreview.not(),
                    panelWeight to arg.weightView,
                    tap_cancel to arg.cancelButton,
                    text_price to arg.cancelButton.not(),
                    panelDetail to arg.ordersPreview
                )
                handlePurpose(map)
                determine(activity, arg, index, tap_agree)
                handleToggle(activity, subjectWeight, arg, hasExpress, tap_cancel)
                updateOrders(activity, index, arg, panelDetail, deliveryRecycler)
            }
        } ?: Navigation.findNavController(view).popBackStack()
    }


    override fun isExpress(express: Boolean) {
        hasExpress = express
    }

}
