package ru.freea.partdeliverapp.upper

object NavigationPurpose {
    val purpose: LinkedHashMap<Int, String> = linkedMapOf(
        1 to "profile",
        2 to "newOrder",
        3 to "confirmOrder",
        4 to "myOrders"
    )

    fun findIndex(i: String): Int {
        return when (i) {
            "newOrder" -> 2
            "confirmOrder" -> 3
            "myOrders" -> 4
            else -> 1
        }
    }

    fun findPurpose(pur: String): PurposeArg {
        return when (pur) {
            "newOrder" -> PurposeArg(
                title = "Новый заказ",
                weightView = true,
                clearButton = true,
                expressDeliver = true,
                whereTaken = true,
                whereDeliver = true,
                pricePanel = true,
                closeIcon = true,
                index = 2
            )
            "confirmOrder" -> PurposeArg(
                title = "Подтверждение заказа",
                pricePanel = true,
                backIcon = true,
                ordersPreview = true,
                cancelButton = true,
                index = 3
            )
            "myOrders" -> PurposeArg(
                title = "Мои заказы",
                backIcon = true,
                ordersPreview = true,
                index = 4
            )
            else -> PurposeArg(index = 1)
        }
    }
}