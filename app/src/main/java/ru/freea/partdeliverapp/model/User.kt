package ru.freea.partdeliverapp.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(
    val id: String?,
    var title: String?,
    var firstName: String?,
    var lastName: String?,
    var gender: String?,
    var email: String?,
    var dateOfBirth: String?,
    var registerDate: String?,
    var phone: String?,
    var picture: String?,
    var location: Location?
) {
    override fun toString(): String {
        return "id = $id, title = $title, firstName = $firstName, lastName = $lastName, gender = $gender" +
                ", email = $email, dateOfBirth = $dateOfBirth, phone = $phone, location = $location"
    }
}