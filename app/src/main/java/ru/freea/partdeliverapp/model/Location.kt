package ru.freea.partdeliverapp.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Location(
    var street: String?,
    var city: String?,
    var state: String?,
    var country: String?,
    var timezone: String?
) {
    override fun toString(): String {
        return "street = $street, city = $city, state = $state, country = $country, timezone = $timezone"
    }
}