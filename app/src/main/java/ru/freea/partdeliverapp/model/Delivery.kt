package ru.freea.partdeliverapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Delivery(
    var addressFrom: String? = null,
    var dateFrom: String? = null,
    var timeFrom: String? = null,
    var commentFrom: String? = null,
    var addressTo: String? = null,
    var dateTo: String? = null,
    var timeTo: String? = null,
    var commentTo: String? = null,
    var price: Double? = null
) : Parcelable {


    override fun toString(): String {
        return "addressFrom = $addressFrom,\n" +
                "dateFrom = $dateFrom,\n" +
                "timeFrom = $timeFrom,\n" +
                "commentFrom = $commentFrom,\n" +
                "addressTo = $addressTo,\n" +
                "dateTo = $dateTo,\n" +
                "timeTo = $timeTo,\n" +
                "commentTo = $commentTo,\n" +
                "price = $price"
    }
}