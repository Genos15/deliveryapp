package ru.freea.partdeliverapp.tools

import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import ru.freea.partdeliverapp.R
import ru.freea.partdeliverapp.model.User

interface UserTools {
    fun loadProfile(
        context: Context?,
        user: User,
        iv: ImageView?,
        un: TextView? = null,
        up: TextView? = null,
        ud: TextView? = null
    ) {
        if (iv == null) return
        context?.let {
            Glide.with(context)
                .load(user.picture)
                .placeholder(R.drawable.ic_account)
                .error(R.drawable.ic_account)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .transform(CircleCrop())
                .into(iv)
        } ?: println("Print message about NULL Context")
        un?.let {
            val t = "${user.title}. ${user.firstName} ${user.lastName}"
            it.text = t
        }
        up?.let {
            val t = "+${user.phone}"
            it.text = t
        }
        ud?.let {
            user.location?.let { l ->
                val a = listOf(l.country, l.state, l.city, l.street)
                val t = a.filterNotNull().joinToString(separator = ",")
                it.text = t
            }
        }
    }

}