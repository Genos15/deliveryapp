package ru.freea.partdeliverapp.tools

import android.app.Activity
import android.widget.Button
import android.widget.Switch
import android.widget.TextView
import androidx.navigation.findNavController
import io.reactivex.rxjava3.subjects.PublishSubject
import ru.freea.partdeliverapp.R
import ru.freea.partdeliverapp.upper.PurposeArg

interface SwitchTools : WeightTools {

    companion object {
        var price: Double? = null
        const val expressAdd = 50.0
    }

    fun handleToggle(
        activity: Activity?,
        subjectWeight: PublishSubject<Double>,
        args: PurposeArg?,
        hasExpress: Boolean,
        tap_cancel: Button?
    ) {
        activity?.let { act ->
            args?.let { arg ->
                if (arg.cancelButton.not()) {
                    act.findViewById<Switch>(R.id.cbExpressDelivery)?.let { sw ->
                        sw.setOnCheckedChangeListener { _, _is ->
                            val v: Double
                            if (_is) {
                                v = expressAdd.plus(price ?: 0.0)
                                setExpress(true)
                            } else {
                                v = (price ?: 0.0)
                                    .minus(price?.let { expressAdd } ?: 0.0)
                                setExpress(false)
                            }
                            subjectWeight.onNext(v)
                            activity.findViewById<TextView>(R.id.text_price)?.let { tp ->
                                tp.text = "${price ?: 0.0}".plus(
                                    act.resources.getString(R.string.rubleSymbol)
                                )
                            }
                        }
                    }
                }
            }
        }

        tap_cancel?.apply {
            setOnClickListener {
                findNavController().popBackStack(
                    R.id.profileConfirmationFragment,
                    false
                )
            }
        }
    }

    fun setSwitchPrice(p: Double?) {
        price = p
    }

}