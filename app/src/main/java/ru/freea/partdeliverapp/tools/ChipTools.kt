package ru.freea.partdeliverapp.tools

import android.annotation.SuppressLint
import android.app.Activity
import android.content.res.ColorStateList
import android.view.View
import androidx.annotation.MainThread
import androidx.core.content.ContextCompat
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import ru.freea.partdeliverapp.R
import ru.freea.partdeliverapp.viewmodel.WeightViewModel
import java.lang.Exception

interface ChipTools {

    companion object {
        var selected: Int? = null
    }

    @MainThread
    @SuppressLint("InflateParams")
    fun loadWeightChips(
        activity: Activity?,
        group: ChipGroup?,
        viewmodel: WeightViewModel? = null
    ) {
        if (activity == null) return

        group?.let {

            val list = listOf(
                "До 1 кг",
                "До 5 кг",
                "До 10 кг",
                "До 15 кг",
                "До 20 кг",
                "До 25 кг",
                "До 30 кг",
                "До 35 кг",
                "До 40 кг",
                "До 45 кг",
                "До 50 кг"
            )

            list.forEach { s ->

                val chip = activity
                    .layoutInflater
                    .inflate(R.layout.item_chip_category, null, false) as Chip

                chip.let {

                    it.text = s

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        it.id = View.generateViewId()
                    } else {
                        it.id = it.text.hashCode()
                    }

                    chipState(it, activity, false)

                    it.setOnClickListener { _ ->
                        selected?.let { s ->
                            try {
                                val c = activity.findViewById<Chip>(s)
                                chipState(c, activity, false)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                        chipState(it, activity, true)
                        viewmodel?.loadWeight(it.text.toString())
                        selected = it.id
                    }

                }

                it.addView(chip)
            }
        }
    }

    @MainThread
    fun chipState(chip: Chip?, activity: Activity?, state: Boolean) {
        if (activity == null) return
        chip?.let {
            if (state) {
                chip.setTextAppearanceResource(R.style.ChipTextAppearanceSelected)
                it.chipBackgroundColor = ColorStateList
                    .valueOf(ContextCompat.getColor(activity, R.color.colorAccentButton))
                it.chipStrokeColor = ColorStateList
                    .valueOf(ContextCompat.getColor(activity, R.color.colorAccentButton))
            } else {
                chip.setTextAppearanceResource(R.style.ChipTextAppearance)
                it.chipBackgroundColor = ColorStateList
                    .valueOf(ContextCompat.getColor(activity, R.color.colorAccent))
                it.chipStrokeColor = ColorStateList
                    .valueOf(ContextCompat.getColor(activity, R.color.colorChipStroke))
            }
        }
    }

}