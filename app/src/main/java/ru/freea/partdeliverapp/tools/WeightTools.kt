package ru.freea.partdeliverapp.tools

import android.app.Activity
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import com.google.android.material.chip.ChipGroup
import io.reactivex.rxjava3.subjects.PublishSubject
import ru.freea.partdeliverapp.R
import ru.freea.partdeliverapp.viewmodel.WeightViewModel
import java.util.*

interface WeightTools : ChipTools {

    companion object {
        private var currentPrice: Double? = null
        const val basePrice: Double = 100.0
        const val expressAdd = 50.0
        var isExpress = false
    }

    fun evaluateWeight(
        activity: Activity?,
        subject: PublishSubject<Double>?,
        modelStore: ViewModelStore
    ) {
        activity?.let { act ->
            act.application?.let { app ->
                val provider = ViewModelProvider
                    .AndroidViewModelFactory
                    .getInstance(app)
                val viewmodel: WeightViewModel? = ViewModelProvider(modelStore, provider)
                    .get(WeightViewModel::class.java)
                viewmodel?.let { vm ->

                    val cg: ChipGroup? = act.findViewById(R.id.chips_weight)

                    cg?.let { loadWeightChips(act, it, vm) }

                    if (act is LifecycleOwner) {
                        vm.currentWeight.observe(act, { weight ->
                            val w = weight
                                .toLowerCase(Locale.ROOT)
                                .replace("до", "")
                                .replace("кг", "")

                            val value = w.trim().toIntOrNull()

                            value?.let { v ->
                                if (v > 0) {

                                    val priceViewer: TextView? = act.findViewById(R.id.text_price)
                                    priceViewer?.apply {
                                        val base = basePrice
                                            .times(v)
                                            .plus(if (isExpress) expressAdd else 0.0)
                                        text = "$base".plus(
                                            context.resources
                                                .getString(R.string.rubleSymbol)
                                        )
                                        currentPrice = base
                                        subject?.onNext(base)
                                    }
                                }
                            }
                        })
                    } else println("The activity is not LifecycleOwner")
                }
            }
        }
    }

    fun setExpress(value: Boolean) {
        isExpress = value
    }

    fun isExpress(express: Boolean)
}