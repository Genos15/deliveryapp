package ru.freea.partdeliverapp.tools

import android.app.Activity
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.freea.partdeliverapp.model.Delivery
import ru.freea.partdeliverapp.persistence.ModelPreferencesManager
import ru.freea.partdeliverapp.upper.PurposeArg

interface OrderTools {

    companion object {
        private var orderDelivery: Delivery? = null
    }

    fun updateOrders(
        activity: Activity?,
        index: Int,
        args: PurposeArg?,
        panelDetail: View?,
        deliveryRecycler: RecyclerView?
    ) {
        activity?.let { act ->
            args?.let { arg ->
                if (arg.ordersPreview) {
                    println("UPDATE ORDERS")
                    Observable.just(index)
                        .map {
                            var data: MutableList<Delivery> = mutableListOf()
                            if (index == 3) {
                                orderDelivery?.let {
                                    data = mutableListOf(it)
                                }
                            } else {
                                data = ModelPreferencesManager.getArray("data")
                            }

                            return@map data
                        }
                        .observeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            panelDetail?.apply {
                                visibility = View.VISIBLE
                            }
                            deliveryRecycler?.apply {
                                layoutManager = LinearLayoutManager(
                                    act, LinearLayoutManager.VERTICAL, false
                                )
                                isNestedScrollingEnabled = false
                                setHasFixedSize(false)
                                adapter = DeliveryAdapter(it.asReversed().toList())
                            }
                        }, { err -> err.printStackTrace() })
                }
            }
        }
    }

    fun setDeliveryForOrder(_delivery: Delivery?) {
        orderDelivery = _delivery
    }
}