package ru.freea.partdeliverapp.tools

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.freea.partdeliverapp.R
import ru.freea.partdeliverapp.model.Delivery

class DeliveryHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.order_container, parent, false)) {

    private var addressFromText: TextView? = null
    private var dateFromText: TextView? = null
    private var addressToText: TextView? = null
    private var dateToText: TextView? = null
    private var priceText: TextView? = null

    init {
        addressFromText = itemView.findViewById(R.id.text_from_address)
        dateFromText = itemView.findViewById(R.id.text_from_date)
        addressToText = itemView.findViewById(R.id.text_to_address)
        dateToText = itemView.findViewById(R.id.text_to_date)
        priceText = itemView.findViewById(R.id.text_final_price)
    }

    fun bind(delivery: Delivery?) {
        delivery?.let {
            addressFromText?.text = it.addressFrom
            dateFromText?.text = it.dateFrom
            addressToText?.text = it.addressTo
            dateToText?.text = it.dateTo
            priceText?.text = it.price?.toString()
        }
    }

}