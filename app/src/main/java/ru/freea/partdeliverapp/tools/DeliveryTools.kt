package ru.freea.partdeliverapp.tools


import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Switch
import android.widget.TextView
import androidx.annotation.MainThread
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import ru.freea.partdeliverapp.R
import ru.freea.partdeliverapp.upper.PurposeArg
import java.text.SimpleDateFormat
import java.util.*

interface DeliveryTools : SwitchTools {
    companion object {
        private const val format = "dd.MM.yyyy"
        private val sdf: SimpleDateFormat? = SimpleDateFormat(format, Locale.ROOT)
        private val calendar: Calendar? = Calendar.getInstance()
    }

    @MainThread
    fun loadPanel(activity: Activity?) {
        activity?.apply act@{
            val map = mutableMapOf<String, View>()
            findViewById<View>(R.id.panel_sender_info)?.let {
                map.put("Откуда забрать", it)
            }
            findViewById<View>(R.id.panel_receiver_info)?.let {
                map.put("Куда доставить", it)
            }
            var number = 0
            for ((k, v) in map) {
                v.findViewById<TextView>(R.id.text_number_panel)
                    ?.apply { text = (++number).toString() }
                v.findViewById<TextView>(R.id.text_name_panel)?.apply { text = k }
                v.findViewById<TextView>(R.id.text_date)?.apply dp@{
                    if (calendar == null) return
                    val dateDialog = OnDateSetListener { _, y, m, d ->
                        calendar.set(Calendar.YEAR, y)
                        calendar.set(Calendar.MONTH, m)
                        calendar.set(Calendar.DAY_OF_MONTH, d)
                        text = sdf?.format(calendar.time)
                    }
                    setOnClickListener {
                        DatePickerDialog(
                            this@act,
                            R.style.datePickerCustom,
                            dateDialog,
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)
                        ).show()
                    }
                    //this is to initialize the TextView in the first launch
                    text = sdf?.format(calendar.time)
                }
                v.findViewById<TextView>(R.id.text_time)?.apply tp@{
                    if (calendar == null) return
                    val timeDialog = TimePickerDialog.OnTimeSetListener { _, h, m ->
                        calendar.set(Calendar.HOUR_OF_DAY, h)
                        calendar.set(Calendar.MINUTE, m)
                        text = h.toString().plus(":").plus(m)
                    }
                    setOnClickListener {
                        TimePickerDialog(
                            this@act,
                            R.style.datePickerCustom,
                            timeDialog,
                            calendar.get(Calendar.HOUR_OF_DAY),
                            calendar.get(Calendar.MINUTE),
                            true
                        ).show()
                    }
                }
            }
        }
    }

    @MainThread
    fun handleToolbar(
        purpose: PurposeArg,
        toolbar: View?,
        toHidePanel: Array<View>? = null,
        toHideTextView: Array<TextView>? = null,
        toUncheckSwitch: Array<Switch>? = null,
        index: Int? = null
    ) {
        toolbar?.let { t ->
            t.findViewById<TextView?>(R.id.text_title_toolbar)?.let {
                it.text = purpose.title
            }
            t.findViewById<ImageButton?>(R.id.tap_close)?.let {
                if (purpose.closeIcon) {
                    it.setOnClickListener { v ->
                        setSwitchPrice(0.0)
                        v.findNavController().popBackStack()
                    }
                } else {
                    it.visibility = View.INVISIBLE
                }
            }
            t.findViewById<ImageButton?>(R.id.tap_back)?.let {
                if (purpose.backIcon) {
                    it.visibility = View.VISIBLE
                    it.setOnClickListener { v ->
                        if (index != null && index > 3) {
                            v.findNavController()
                                .popBackStack(R.id.profileConfirmationFragment, false)
                        } else Navigation.findNavController(v).popBackStack()
                    }
                } else {
                    it.visibility = View.INVISIBLE
                }
            }
            toolbar.findViewById<TextView?>(R.id.text_clear)?.let {
                if (purpose.clearButton) {
                    it.setOnClickListener {
                        println("Trying to Clear all")

                        toHidePanel?.apply {
                            clearPanel(arrayOf(*this))
                        }
                        toHideTextView?.apply {
                            clearTextView(arrayOf(*this))
                        }
                        toUncheckSwitch?.apply {
                            unCheckSwitch(arrayOf(*this))
                        }
                    }
                } else {
                    it.visibility = View.GONE
                }
            }
        }
    }

    @MainThread
    fun handlePurpose(map: Map<View?, Boolean>) {
        map.let {
            for ((k, v) in it) {
                if (v.not()) k?.visibility = View.INVISIBLE
            }
        }
    }

    @MainThread
    fun clearPanel(panel: Array<View>?) {
        if (panel == null) return
        for (v in panel) {
            v.findViewById<EditText>(R.id.text_address)?.apply {
                text.clear()
            }
            v.findViewById<EditText>(R.id.text_date)?.apply {
                text.clear()
            }
            v.findViewById<EditText>(R.id.text_time)?.apply {
                text.clear()
            }
            v.findViewById<EditText>(R.id.text_comment)?.apply {
                text.clear()
            }
        }
    }

    fun clearTextView(panel: Array<TextView>) {
        for (v in panel) {
            v.apply { text = "" }
        }
    }

    fun unCheckSwitch(panel: Array<Switch>) {
        for (v in panel) {
            v.apply { isChecked = false }
        }
    }

}