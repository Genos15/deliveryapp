package ru.freea.partdeliverapp.tools

import ru.freea.partdeliverapp.model.User

interface UserNotifier {
    fun notify(user: User?)
}