package ru.freea.partdeliverapp.tools

import android.app.Activity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.MainThread
import androidx.navigation.Navigation
import kotlinx.coroutines.runBlocking
import ru.freea.partdeliverapp.R
import ru.freea.partdeliverapp.model.Delivery
import ru.freea.partdeliverapp.persistence.ModelPreferencesManager
import ru.freea.partdeliverapp.upper.NavigationPurpose
import ru.freea.partdeliverapp.upper.OrderProcessFragmentDirections
import ru.freea.partdeliverapp.upper.PurposeArg

interface NextPanelBehavior {

    companion object {
        private var price: Double = 0.0
        private var nextDelivery: Delivery? = null
    }

    @MainThread
    fun determine(activity: Activity?, args: PurposeArg?, index: Int, tap_agree: Button?) =
        runBlocking {
            args?.let { arg ->
                if (arg.pricePanel) {
                    activity?.let { act ->
                        tap_agree?.let {
                            it.setOnClickListener {

                                println("Next button has been clicked with index=$index")
                                var deliver: Delivery? = null

                                val reject = reasonReject(
                                    toHidePanel = arrayOf(
                                        activity.findViewById(R.id.panel_sender_info),
                                        activity.findViewById(R.id.panel_receiver_info)
                                    ),
                                    toHideTextView = arrayOf(activity.findViewById(R.id.text_price))
                                )
                                when (index) {
                                    1 -> {
                                        if (reject) {
                                            Toast.makeText(
                                                act,
                                                "Impossible to proceed. You need fill all required fields",
                                                Toast.LENGTH_LONG
                                            ).show()
                                            return@setOnClickListener
                                        }
                                    }
                                    2, 3, 4 -> {
                                        if (index == 2) {
                                            if (reject) {
                                                Toast.makeText(
                                                    act,
                                                    "Impossible to proceed. You need fill all required fields",
                                                    Toast.LENGTH_LONG
                                                ).show()
                                                return@setOnClickListener
                                            }
                                        }

                                        if (arg.expressDeliver) {
                                            deliver = collect(act)
                                            deliver.price = price
                                        }

                                        if (index == 3) {
                                            val data = ModelPreferencesManager.getArray("data")
                                            nextDelivery?.let { d -> data.add(d) }
                                            ModelPreferencesManager.put(data, "data")
                                        }
                                    }
                                }

                                NavigationPurpose.purpose[index + 1]?.let { p ->
                                    val action = OrderProcessFragmentDirections
                                        .actionOrderSelf(
                                            nextPurpose = p,
                                            delivery = deliver ?: nextDelivery
                                        )
                                    Navigation.findNavController(it)
                                        .navigate(action)
                                }
                            }
                        }
                    }
                }
            }
        }

    private fun reasonReject(
        toHidePanel: Array<View>? = null,
        toHideTextView: Array<TextView>? = null
    ): Boolean {
        toHidePanel?.apply p@{
            loop@ for (t in this) {
                t.apply parent@{
                    findViewById<EditText>(R.id.text_address)?.apply address@{
                        if (visibility == View.VISIBLE && text.isNullOrEmpty()) return true
                    }
                    findViewById<EditText>(R.id.text_date)?.apply date@{
                        if (visibility == View.VISIBLE && text.isNullOrEmpty()) return true
                    }
                }
            }
        }
        toHideTextView?.apply {
            for (t in this) {
                t.apply {
                    if (visibility == View.VISIBLE && text.isNullOrEmpty()) return true
                }
            }
        }
        return false
    }

    private fun collect(activity: Activity?): Delivery {
        val delivery = Delivery()
        activity?.apply act@{
            val sender = findViewById<View>(R.id.panel_sender_info)
            val receiver = findViewById<View>(R.id.panel_receiver_info)
            sender?.apply sender@{
                findViewById<EditText>(R.id.text_address)?.apply {
                    delivery.addressFrom = text.toString()
                }
                findViewById<EditText>(R.id.text_date)?.apply {
                    delivery.dateFrom = text.toString()
                }
                findViewById<EditText>(R.id.text_time)?.apply {
                    delivery.timeFrom = text.toString()
                }
                findViewById<EditText>(R.id.text_comment)?.apply {
                    delivery.commentFrom = text.toString()
                }
            }
            receiver?.apply receiver@{
                findViewById<EditText>(R.id.text_address)?.apply {
                    delivery.addressTo = text.toString()
                }
                findViewById<EditText>(R.id.text_date)?.apply {
                    delivery.dateTo = text.toString()
                }
                findViewById<EditText>(R.id.text_time)?.apply {
                    delivery.timeTo = text.toString()
                }
                findViewById<EditText>(R.id.text_comment)?.apply {
                    delivery.commentTo = text.toString()
                }
            }
        }
        return delivery
    }

    fun setPrice(p: Double?) {
        p?.let { price = it }
    }

    fun setNextDelivery(d: Delivery?) {
        d?.let { nextDelivery = it }
    }
}