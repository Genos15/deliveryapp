package ru.freea.partdeliverapp.tools

interface UserObserver {
    fun observe()
}