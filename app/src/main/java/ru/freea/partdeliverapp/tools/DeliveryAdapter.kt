package ru.freea.partdeliverapp.tools

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.freea.partdeliverapp.model.Delivery

class DeliveryAdapter(private var list: List<Delivery>?) : RecyclerView.Adapter<DeliveryHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeliveryHolder {
        val inflater = LayoutInflater.from(parent.context)
        return DeliveryHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: DeliveryHolder, position: Int) {
        val h: DeliveryHolder? = holder
        h?.let { _h ->
            list?.let {
                val delivery = it[position]
                _h.bind(delivery)
            }
        }
    }

    override fun getItemCount(): Int {
        return list?.let {
            return@let it.size
        } ?: 0
    }
}