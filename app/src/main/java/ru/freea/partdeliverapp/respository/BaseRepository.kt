package ru.freea.partdeliverapp.respository

import retrofit2.Response
import ru.freea.partdeliverapp.io.ResultRequest
import java.io.IOException

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, errorMessage: String): T? {
        val result: ResultRequest<T> = safeApiResult(call, errorMessage)
        var data: T? = null

        when (result) {
            is ResultRequest.Success ->
                data = result.data
            is ResultRequest.Error -> {
                println("$errorMessage & Exception - ${result.exception}")
            }
        }
        return data
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> Response<T>,
        errorMessage: String
    ): ResultRequest<T> {
        val response = call.invoke()
        if (response.isSuccessful) return ResultRequest.Success(response.body()!!)

        println(" message = ${response.message()}")
        println(" bodyError = ${response.errorBody()?.source()}")
        println(" bodyError = ${response.errorBody()?.string()}")

        return ResultRequest.Error(IOException("Error Occurred during getting safe Api result, Custom ERROR - $errorMessage"))
    }
}