package ru.freea.partdeliverapp.respository

import ru.freea.partdeliverapp.BuildConfig
import ru.freea.partdeliverapp.io.UserApi
import ru.freea.partdeliverapp.model.User

class UserRepository(private val api: UserApi) : BaseRepository() {

    suspend fun getUser(): User? {
        return safeApiCall(
            call = { api.getUserAsync(BuildConfig.USER_ID).await() },
            errorMessage = "Impossible to access user profile"
        )
    }
}