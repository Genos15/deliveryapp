package ru.freea.partdeliverapp

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import ru.freea.partdeliverapp.persistence.ModelPreferencesManager
import ru.freea.partdeliverapp.tools.UserNotifier
import ru.freea.partdeliverapp.tools.UserObserver
import ru.freea.partdeliverapp.viewmodel.UserViewModel

class MainActivity : AppCompatActivity(), UserObserver {

    var notifier: UserNotifier? = null
    private var viewmodel: UserViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            window?.let {
                it.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                it.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                it.statusBarColor = ContextCompat.getColor(this, R.color.colorStatusBar)
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    it.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                } else {
                    it.statusBarColor = ContextCompat.getColor(this, R.color.colorStatusBarDown)
                }
            }
        }

        ModelPreferencesManager.with(application)

        application?.let {
            val provider = ViewModelProvider
                .AndroidViewModelFactory
                .getInstance(it)
            viewmodel = ViewModelProvider(viewModelStore, provider)
                .get(UserViewModel::class.java)
            viewmodel?.apply { fetchCurrentUser() }
        }
    }

    override fun observe() {
        println("reload user profile")
        viewmodel?.apply {
            currentUser.observe(this@MainActivity, { user ->
                user?.let {
                    notifier?.apply {
                        notify(it)
                    }
                }
            })
        }
    }
}
