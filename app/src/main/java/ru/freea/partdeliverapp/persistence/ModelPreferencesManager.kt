package ru.freea.partdeliverapp.persistence

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder
import org.json.JSONArray
import org.json.JSONObject
import ru.freea.partdeliverapp.model.Delivery


object ModelPreferencesManager {

    lateinit var preferences: SharedPreferences
    private const val PREFERENCES_FILE_NAME = "PREFERENCES_FILE_NAME"
    fun with(application: Application) {
        preferences = application.getSharedPreferences(
            PREFERENCES_FILE_NAME, Context.MODE_PRIVATE
        )
    }

    fun <T> put(`object`: T, key: String) {
        try {
            val jsonString = GsonBuilder().create().toJson(`object`)
            preferences.edit().putString(key, jsonString).apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    inline fun <reified T> get(key: String): T? {
        return try {
            val value = preferences.getString(key, null)
            return GsonBuilder().create().fromJson(value, T::class.java)
        } catch (e: Exception) {
            null
        }
    }

    fun getArray(key: String?): MutableList<Delivery> {
        return try {
            val begin = System.nanoTime()

            val value = preferences.getString(key, null)
            val array = JSONArray(value)
            println(array.toString(4))
            val final = mutableListOf<Delivery>()

            val mid = System.nanoTime()
            println("Middle Time in nanoseconds : ${(mid - begin) / 1_000_000_000.0}")

//            myCollection.parallelStream()
//                .map { ... }
//                .filter { ... }
            for (i in 0 until array.length()) {
                val d = Delivery()
                val elt: JSONObject = array.getJSONObject(i)
                d.addressFrom = elt.getString("addressFrom")
                d.addressTo = elt.getString("addressTo")
                d.commentFrom = elt.getString("commentFrom")
                d.dateFrom = elt.getString("dateFrom")
                d.commentTo = elt.getString("commentTo")
                d.dateTo = elt.getString("dateTo")
                d.timeFrom = elt.getString("timeFrom")
                d.timeTo = elt.getString("timeTo")
                d.price = elt.getDouble("price")
                final.add(d)
            }
            val end = System.nanoTime()
            println("Elapsed Time in nanoseconds : ${(end - begin) / 1_000_000_000.0}")
            return final
        } catch (e: Exception) {
            mutableListOf()
        }
    }


}