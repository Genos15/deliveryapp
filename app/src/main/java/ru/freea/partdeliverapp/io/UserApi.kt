package ru.freea.partdeliverapp.io

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import ru.freea.partdeliverapp.BuildConfig
import ru.freea.partdeliverapp.model.User

interface UserApi {
    @Headers("app-id: ${BuildConfig.APP_PROFILE_ID}")
    @GET("{id}")
    fun getUserAsync(@Path("id") id: String): Deferred<Response<User>>
}