package ru.freea.partdeliverapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ru.freea.partdeliverapp.model.User
import ru.freea.partdeliverapp.respository.UserRepository
import kotlin.coroutines.CoroutineContext

class UserViewModel : ViewModel() {
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)
    private val repository: UserRepository = UserRepository(ApiFactory.userApi)
    val currentUser = MutableLiveData<User?>()

    fun fetchCurrentUser() {
        scope.launch {
            val user = repository.getUser()
            currentUser.postValue(user)
        }
    }

    fun cancelAllRequests() = coroutineContext.cancel()
}