package ru.freea.partdeliverapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ru.freea.partdeliverapp.model.User
import ru.freea.partdeliverapp.respository.UserRepository
import kotlin.coroutines.CoroutineContext

class WeightViewModel : ViewModel() {
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)

    val currentWeight = MutableLiveData<String>()

    fun loadWeight(weight: String?) {
        weight?.let {
            scope.launch {
                currentWeight.postValue(weight)
            }
        }
    }
}